package com.crud.dima.service;

import com.crud.dima.model.*;
import java.util.List;

public interface UsersService {
    public List<Users> getAllUsers();

    public Users getUsersById(long id);

    public void saveOrUpdate(Users users);

    public void deleteUsers(long id);
}
