package com.crud.dima.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.crud.dima.model.Users;
import com.crud.dima.repository.UsersRepository;

@Service
@Transactional
public class UsersServiceImpl implements UsersService {
    @Autowired
    UsersRepository usersRepository;

    @Override
    public List<Users> getAllUsers() {
        return (List<Users>) usersRepository.findAll();
    }

    @Override
    public Users getUsersById(long id) {
        return usersRepository.findById(id).get();
    }

    @Override
    public void saveOrUpdate(Users users) {
        usersRepository.save(users);
    }

    @Override
    public void deleteUsers(long id) {
        usersRepository.deleteById(id);
    }
}
